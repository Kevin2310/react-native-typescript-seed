import React, {Component} from 'react';
import HomeScreen from "./screens/Home/HomeScreen";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <HomeScreen/>
    );
  }
}