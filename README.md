# react-native-typescript-seed

seed for react-native with typescript


## How To Use

1. rename project in `package.json` und `app.json`
2. run `yarn install`
3. run `react-native eject`

Then you are ready to run app with `npm run ios` or `npm run android`